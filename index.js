 class Product {
   constructor(name,model,price){
     this.name = name,
     this.model = model,
     this.price = price


   }

 };
class ProducInterface{
  addProduct(newProduct){
  const view =  document.getElementById('list-products');
  const card = document.createElement('div');
  card.innerHTML = `<div class="card-body text-center  mb-4">
  <strong> Product: </strong> ${ newProduct.name }
  <b> Model: </b>${ newProduct.model }
  <b> Price: </b>${ newProduct.price }
  <a href="#" class= "btn btn-danger ml-3" name="borrar" >Delete</a>
  </div>`
  view.appendChild(card);
  };
  resetForm(){
      document.getElementById('product-form').reset();
};
  deleteProduct(e){
    if(e.name === "borrar"){
   const elemento = e.parentElement;
  elemento.remove();
    }
  };
  showMessage(mensaje, css){
  const appContent =  document.getElementById('appContent');
    const container = document.querySelector('.container');
    const div = document.createElement('div');
    div.className = `alert alert-${css}`;
    div.appendChild(document.createTextNode(mensaje));
    container.insertBefore(div,appContent);

    setTimeout(()=>{
       //seleccionamos elementos con la clase alert y lo removemos en 2 segundos.
       document.querySelector('.alert').remove();

},2000)

  };

}

 document.getElementById('product-form').addEventListener('submit',(e)=>{
   let name = document.getElementById('name').value;
   let model = document.getElementById('model').value;
   let price = document.getElementById('price').value;
  const  productInterface = new ProducInterface();
if( name ==='' || model ==='' || price ===''){
  productInterface.showMessage('complete campos', 'info');

} else {
  const newProduct = new Product( name,model,price);

 productInterface.addProduct(newProduct);
 productInterface.showMessage('agregado', 'success');
 productInterface.resetForm();

}


   e.preventDefault();
 });
 document.getElementById('list-products').addEventListener('click', (e)=>{
   const  productInterface = new ProducInterface();
   productInterface.deleteProduct(e.target);
   productInterface.showMessage('borrado', 'warning');

 });
